import {theme} from '@chakra-ui/core'

export default {
    ...theme,
    colors: {
        ...theme.colors,
        scratchBlue: {
            500:"#245CA6",
            600:"#245CA6"
        }
    }
}
