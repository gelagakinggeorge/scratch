import React from "react";
import { Grid } from "@chakra-ui/core";
import UserCard from "./UserCard";

export const UserList = ({ users, primaryFn, secondaryFn }) => (
  <Grid templateColumns="repeat(3, 1fr)" gap={6}>
    {users.map(user => (
      <UserCard
        user={user}
        key={user.id}
        primaryFn={primaryFn}
        secondaryFn={secondaryFn}
      />
    ))}
  </Grid>
);
