import React from "react";
import { Box, Badge, Heading, IconButton, Text, Flex } from "@chakra-ui/core";
import { UserStatus } from "../constants";

/**
 * Card containing user details along with action buttons.
 * @param {Object} user - The user whose details are to be displayed on the card
 * @param {} primaryFn - The cards primary function
 * @param {} secondaryFn - The cards secondary function
 */

function UserCard({ user, primaryFn, secondaryFn }) {
  return (
    <Box maxW="md" borderWidth="1px" rounded="lg" overflow="hidden" bg="white">
      <Box p="6">
        <Box d="flex" alignItems="baseline">
          <Box mt="1" fontWeight="semibold" as="h2" lineHeight="tight">
            {user.id}
          </Box>
          {user.status === UserStatus.ACTIVE ? (
            <Badge rounded="full" px="2" variantColor="teal" marginLeft="auto">
              {user.status}
            </Badge>
          ) : (
            <Badge rounded="full" px="2" variantColor="red" marginLeft="auto">
              {user.status}
            </Badge>
          )}
        </Box>
        <Box mt="1" lineHeight="tight">
          <Heading>
            {user.firstName} {user.lastName}
          </Heading>
        </Box>

        <Box d="flex" alignItems="baseline">
          <Text fontSize="md">{user.email}</Text>
        </Box>

        <Box>
          <Box
            as="span"
            color="gray.600"
            fontSize="md"
            textTransform="capitalize"
          >
            Role - <Text as="mark" bg="#F5B81C"> {user.role}</Text>
          </Box>
        </Box>

        <Flex justifyContent="start" alignItems="center">
          <IconButton
            size="sm"
            icon="edit"
            mt={4}
            mr={4}
            onClick={() => primaryFn(user)}
          />
          <IconButton
            size="sm"
            icon="delete"
            mt={4}
            onClick={() => secondaryFn(user)}
          />
        </Flex>
      </Box>
    </Box>
  );
}

export default UserCard;
