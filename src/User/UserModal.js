import React from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Input,
  Button,
  Select,
  FormErrorMessage
} from "@chakra-ui/core";
import { Formik, Field } from "formik";
import { UserStatus } from "../constants";

/**
 * User Modal - UI with which we add and edit users
 *
 * @param {boolean} isOpen - Is this modal open?
 * @param {onClose} onClose - Function to close this modal
 * @param {} primaryFn - This modals primary function (adding/editing)
 * @param {Object} user
 * @param {errors} errros - Any errors that have came up after validation
 * @param {string} title - Modal title
 */

function UserModal({ isOpen, onClose, primaryFn, user, errors, title }) {
  const initialRef = React.useRef();
  const finalRef = React.useRef();

  return (
    <>
      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{title}</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <Formik
              initialValues={user}
              onSubmit={(values, actions) => {
                actions.setSubmitting(false);
                primaryFn(values);
              }}
              render={props => (
                <form onSubmit={props.handleSubmit}>
                  <Field name="firstName">
                    {props => (
                      <FormControl>
                        <FormLabel>First name</FormLabel>
                        <Input
                          {...props.field}
                          id="firstName"
                          ref={initialRef}
                          placeholder="First name"
                        />
                      </FormControl>
                    )}
                  </Field>
                  <Field name="lastName">
                    {props => (
                      <FormControl mt={4}>
                        <FormLabel>Last name</FormLabel>
                        <Input
                          {...props.field}
                          id="lastName"
                          placeholder="Last name"
                        />
                      </FormControl>
                    )}
                  </Field>
                  <Field name="email">
                    {props => (
                      <FormControl
                        mt={4}
                        isInvalid={errors.email && props.form.touched.email}
                      >
                        <FormLabel>Email</FormLabel>
                        <Input
                          {...props.field}
                          id="email"
                          type="email"
                          placeholder="Email"
                        />
                        <FormErrorMessage>{errors.email}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>
                  <Field name="role">
                    {props => (
                      <FormControl mt={4}>
                        <FormLabel htmlFor="role">Role</FormLabel>
                        <Select
                          id="role"
                          placeholder="Select role"
                          defaultValue={props.field.value}
                          onChange={props.field.onChange}
                        >
                          <option value="Doctor">Doctor</option>
                          <option value="Admin">Admin</option>
                          <option value="Accountant">Accountant</option>
                        </Select>
                      </FormControl>
                    )}
                  </Field>
                  <Field name="status">
                    {props => (
                      <FormControl mt={4}>
                        <FormLabel htmlFor="status">Status</FormLabel>
                        <Select
                          id="status"
                          placeholder="Select status"
                          defaultValue={props.field.value}
                          onChange={props.field.onChange}
                        >
                          {Object.entries(UserStatus).map(([key, value]) => (
                            <option value={value} key={key}>{value}</option>
                          ))}
                        </Select>
                      </FormControl>
                    )}
                  </Field>
                  <Button
                    mt={4}
                    variantColor="scratchBlue"
                    isLoading={props.isSubmitting}
                    type="submit"
                  >
                    Save
                  </Button>
                  <Button mt={4} ml={4} onClick={onClose}>
                    Cancel
                  </Button>
                </form>
              )}
            />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}

export default UserModal;
