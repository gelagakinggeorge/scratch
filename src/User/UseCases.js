import utils from '../utils'
import {
    Mode
} from "../constants";

/**
 * The different use cases that we have for our users
 * 
 * @param {Object} state - Current state of the app
 * @param {} setState - Function that alters the state of the app
 * @param {} onOpen - Function that opens the user modal
 * @param {} onClose - Funciton that closes the user modal
 */

export default function UseCases({
    state,
    setState,
    onOpen,
    onClose
}) {
    return Object.freeze({
        editUser,
        addUser,
        updateUser,
        deleteUser
    })

    /**
     * Opens the User modal so selected user can be edited
     * @param {Object} user 
     */
    function editUser(user) {
        setState({
            ...state,
            currentUser: user,
            mode: Mode.EDITING,
            errors: {}
        });

        onOpen();
    }

    /**
     * Adds a new user 
     * @param {Object} user 
     */
    function addUser(user) {
        let {
            firstName,
            lastName,
            email,
            role,
            status
        } = user
        let data = state.data;
        const id = state.data.length;

        if (utils.hasDuplcateEmail({
                users: data,
                email
            })) {
            setState({
                ...state,
                errors: {
                    email: "Sorry this email already exists"
                }
            });
            return;
        }

        data.push({
            id,
            firstName,
            lastName,
            email,
            role,
            status
        });

        setState({
            data,
            currentUser: utils.getEmptyUserObj(),
            mode: Mode.VIEWING,
            errors: {}
        });

        onClose(); //close the modal 
    }

    /**
     * Updates a selected user
     * @param {Object} updatedUser 
     */
    function updateUser(updatedUser) {
        const data = state.data;
        const newData = data.map(user => {
            if (updatedUser.id === user.id) {
                return {
                    ...updatedUser
                };
            }
            return user;
        });

        // Validate this users email against all other users.
        const listWithoutUser = data.filter(user => user.id !== updatedUser.id)
        if (utils.hasDuplcateEmail({
                users: listWithoutUser,
                email: updatedUser.email,
                exceptions: [updatedUser.email]
            })) {
            setState({
                ...state,
                errors: {
                    email: "Sorry this email already exists"
                }
            });
            return;
        }

        setState({
            data: newData,
            currentUser: utils.getEmptyUserObj(),
            mode: Mode.VIEWING,
            errors: {}
        });

        onClose(); //close the modal
    }

    /**
     * Deletes a user
     * @param {Object} userToDelete 
     */
    function deleteUser(userToDelete) {
        const data = state.data;
        const newData = data.filter(user => user.id !== userToDelete.id);

        setState({
            data: newData,
            currentUser: utils.getEmptyUserObj(),
            mode: Mode.VIEWING,
            errors: {}
        });
    }
}