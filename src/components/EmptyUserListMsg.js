import React from "react";
import paw from "../paw_symbol.png";
import { Box, Heading, Image } from "@chakra-ui/core";

export const EmptyUserListMsg = () => (
  <Box textAlign="center" mt={150}>
    <Image size="50px" ml="auto" mr="auto" src={paw}></Image>
    <Heading as="h1" fontSize="2xl" color="#245CA6" p={2}>
      Let's add some users :)
    </Heading>
  </Box>
);
