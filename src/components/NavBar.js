import React from "react";
import { Box, Flex, Image, Button} from "@chakra-ui/core";
import logo from "../logo.png";

function NavBar({onOpen}) {
  return (
    <Flex
      bg="#fff"
      w="100%"
      px={5}
      py={4}
      justifyContent="space-between"
      alignItems="center"
    >
      <Flex flexDirection="row" justifyContent="center" alignItems="center">
        <Image src={logo} htmlWidth={100} htmlHeight={100} />
      </Flex>
      <Box>
        <Button 
            rightIcon="add" 
            variantColor="#245CA6"
            bg="#245CA6"
            variant="solid" 
            size="sm"
            onClick={onOpen}
            >
          Add User
        </Button>
      </Box>
    </Flex>
  );
}

export default NavBar;
