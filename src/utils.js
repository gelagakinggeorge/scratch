import {
    Mode
} from "./constants";

export default Object.freeze({
    hasDuplcateEmail,
    getEmptyUserObj,
    closeModal
})

function hasDuplcateEmail({
    users,
    email
}) {
    let seen = [];
    for (let user of users) {
        seen.push(user.email);

        if (seen.includes(email)) {
            return true;
        }
    }

    return false;
};

function getEmptyUserObj() {
    return {
        firstName: "",
        lastName: "",
        email: "",
        role: "",
        status: ""
    }
}

function closeModal({
    setState,
    onClose,
    state
}) {
    setState({
        ...state,
        currentUser: getEmptyUserObj(),
        mode: Mode.VIEWING
    });
    onClose();
};