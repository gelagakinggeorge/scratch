/**
 * Mode - Whether the user is currently editing a user or viewing results
 */
export const Mode = Object.freeze({
        VIEWING: 0,
        EDITING: 1
})

/**
 * UserStatus - The different options for a users status
 */
export const UserStatus = Object.freeze({
    ACTIVE: "Active",
    INACTIVE: "Inactive"
})