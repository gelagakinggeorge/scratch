import React, { useState } from "react";
import {
  ThemeProvider,
  CSSReset,
  Box,
  useDisclosure
} from "@chakra-ui/core";
import theme from "./theme";

import NavBar from "./components/NavBar";
import { EmptyUserListMsg } from "./components/EmptyUserListMsg";
import { UserList } from "./User/UserList";
import UserModal from "./User/UserModal";
import UseCases from "./User/UseCases";

import { Mode } from "./constants";
import utils from "./utils";

function App() {
  const [state, setState] = useState({
    data: [],
    currentUser: utils.getEmptyUserObj(),
    mode: Mode.VIEWING,
    errors: {}
  });

  const { isOpen, onOpen, onClose } = useDisclosure();
  const usecases = UseCases({ state, setState, onOpen, onClose });

  return (
    <ThemeProvider theme={theme}>
      <CSSReset />
      <NavBar onOpen={onOpen} />
      <UserModal
        isOpen={isOpen}
        onClose={() => utils.closeModal({setState, state, onClose})}
        primaryFn={
          state.mode === Mode.EDITING ? usecases.updateUser : usecases.addUser
        }
        user={state.currentUser}
        errors={state.errors}
        title={state.mode === Mode.EDITING ? "Edit User" : "Add User"}
      />

      <Box p={6}>
        {state.data.length < 1 && <EmptyUserListMsg />}

        <UserList
          users={state.data}
          primaryFn={usecases.editUser}
          secondaryFn={usecases.deleteUser}
        />
      </Box>
    </ThemeProvider>
  );
}

export default App;
